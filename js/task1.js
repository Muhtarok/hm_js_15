"use strict";
/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. 
Новий текст повинен вказувати, що операція виконана успішно.

*/

console.log("THEORY");

console.log("1. setInterval запускає якісь дії через заданий інтервал регулярно, setTimeout робить це разово");
console.log("2. clearInterval/clearTimeout");


console.log("PRACTICE");

console.log("TASK 1");

// let timerId = setTimeout(changeText, 3000);

let button = document.querySelector("#button");
// console.log(button);
button.addEventListener("click", function () {
    setTimeout(function () {
        document.querySelector("#text-for-changing").innerText = "Mission is successfully completed";
    }, 3000);
});


