"use strict";

/* Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. 
При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено". */

// const timerFunction = function () {



/* for (let i = 10; i >= 1; i = i - 1) {    //перша спроба

    setInterval(function () {
        let timer = document.querySelector("#task2").innerText = `Timer for TASK 2: ${i}`;
        console.log(timer);
    }, 3000);

    if (i = 1) {
        timer = document.querySelector("#task2").innerText = `Зворотній відлік завершен`;
    }
} */

let i = 10;

const timerInterval = setInterval(() => {
    let timer = document.querySelector("#task2").innerText = `Timer for TASK 2: ${i}`;
    i = i - 1;
    console.log(timer);
    if (i < 1) {
        document.querySelector("#task2").innerText = `Зворотній відлік завершено`;
        clearInterval(timerInterval);
    }
    
}, 1100);


